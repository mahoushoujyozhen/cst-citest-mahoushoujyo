package article

//author: {"name":"article","email":"389403710@qq.com"}
//annotation:article-service

import (
	"backend/cmn"
	"backend/db"
	"backend/zap_log"
	"context"
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"go.uber.org/zap"
	"net/http"
)

type articleStruct struct {
	UserId    int    `json:"user_id"`
	Content   string `json:"content"`
	ArticleId int    `json:"article_id"`
	Method    string `json:"method"`
}
type resp struct {
	Status int           `json:"status"`
	Msg    string        `json:"msg"`
	Data   articleStruct `json:"data"`
}

func Enroll(author string) {
	fmt.Println("login enroll call! ")
	var developer *cmn.ModuleAuthor
	if author != "" {
		var d cmn.ModuleAuthor
		err := json.Unmarshal([]byte(author), &d)
		if err != nil {
			//z.Error(err.Error())
			fmt.Println(err.Error())
			return
		}
		developer = &d
	}
	ep := cmn.ServeEndPoint{
		Fn:   article,
		Path: "/api/article",
		Name: "article",
	}
	err := cmn.AddService(&ep)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(developer)
}

func article(w http.ResponseWriter, r *http.Request) {
	fmt.Println("article service")
	var req = cmn.ReplyProto{
		Status: 0,
		Msg:    "success",
	}
	log := zap_log.Log
	dbConn, err := db.GetDBConn()
	if err != nil {
		log.Error(fmt.Sprintf("err:%v", err))
		fmt.Println(err)
		req.Status = 566
		req.Msg = "db Pool has no conn"
		cmn.Resp(w, &req)
		return
	}
	defer db.Close(dbConn)
	//这里写业务函数
	var temp = articleStruct{}
	err = json.NewDecoder(r.Body).Decode(&temp)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 777
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, &req)
		return
	}
	fmt.Println("userId:", temp.UserId)
	fmt.Println("articleId:", temp.ArticleId)
	fmt.Println("method:", temp.Method)
	fmt.Println("content", temp.Content)

	switch temp.Method {
	//更改publish的状态为true
	case "publish":
		articlePublish(w, log, &req, dbConn, &temp)

	//	不改变publish的状态
	case "save":
		articleSave(w, log, &req, dbConn, &temp)

	case "GET":
		articleShow(w, log, &req, dbConn, &temp)
	}

}

func articlePublish(w http.ResponseWriter, log *zap.Logger, req *cmn.ReplyProto, dbConn *pgxpool.Conn, article *articleStruct) {
	if article.ArticleId == 0 {
		//首次发布
		s := `INSERT INTO article (content,user_id,publish) VALUES ($1,$2,$3);`
		_, err := dbConn.Exec(context.Background(), s, article.Content, article.UserId, true)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 778
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}
		var articleId int
		//查找出最新产生的那条数据（article_id按照降序排列）
		s = `SELECT article_id FROM article WHERE  user_id = $1
			order by article_id DESC ;`
		rs := dbConn.QueryRow(context.Background(), s, article.UserId)
		err = rs.Scan(&articleId)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 779
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}

		var resArticle = articleStruct{
			ArticleId: articleId,
		}
		var resp = resp{
			Status: 0,
			Msg:    "发布成功",
			Data:   resArticle,
		}
		buf, err := json.Marshal(resp)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 777
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}
		fmt.Println(string(buf))
		w.Write(buf)
		return
	}
	//发布更新
	s := `UPDATE article 
		SET content = $1 , publish = $2  WHERE article_id = $3;`
	_, err := dbConn.Exec(context.Background(), s, article.Content, true, article.ArticleId)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 778
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}

	var resp = resp{
		Status: 0,
		Msg:    "发布成功",
	}
	buf, err := json.Marshal(resp)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 777
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}
	fmt.Println(string(buf))
	w.Write(buf)
	return
}
func articleSave(w http.ResponseWriter, log *zap.Logger, req *cmn.ReplyProto, dbConn *pgxpool.Conn, article *articleStruct) {
	if article.ArticleId == 0 {
		//首次保存
		s := `INSERT INTO article (content,user_id,publish) VALUES ($1,$2,$3);`
		_, err := dbConn.Exec(context.Background(), s, article.Content, article.UserId, false)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 778
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}
		var articleId int
		//之后修改为查找出最新产生的那条数据（article_id按照降序排列）
		s = `SELECT article_id FROM article WHERE  user_id = $1
			order by article_id DESC ;`
		rs := dbConn.QueryRow(context.Background(), s, article.UserId)
		err = rs.Scan(&articleId)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 779
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}

		var resArticle = articleStruct{
			ArticleId: articleId,
		}
		var resp = resp{
			Status: 0,
			Msg:    "发布成功",
			Data:   resArticle,
		}
		buf, err := json.Marshal(resp)
		if err != nil {
			log.Error(fmt.Sprintf("err: %s", err))
			req.Status = 777
			req.Msg = fmt.Sprintf("err: %s", err)
			cmn.Resp(w, req)
			return
		}
		fmt.Println(string(buf))
		w.Write(buf)
		return
	}
	//发布更新
	s := `UPDATE article 
		SET content = $1 WHERE article_id = $2;`
	_, err := dbConn.Exec(context.Background(), s, article.Content, article.ArticleId)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 778
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}

	var resp = resp{
		Status: 0,
		Msg:    "发布成功",
	}
	buf, err := json.Marshal(resp)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 777
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}
	fmt.Println(string(buf))
	w.Write(buf)
	return
}

//return article
func articleShow(w http.ResponseWriter, log *zap.Logger, req *cmn.ReplyProto, dbConn *pgxpool.Conn, article *articleStruct) {
	//	use article_id to search content
	s := `SELECT content FROM article WHERE article_id = $1;`
	rs := dbConn.QueryRow(context.Background(), s, article.ArticleId)
	var resArticle = articleStruct{}
	err := rs.Scan(&resArticle.Content)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 778
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}
	var resp = resp{
		Status: 0,
		Msg:    "展示成功",
		Data:   resArticle,
	}
	buf, err := json.Marshal(resp)
	if err != nil {
		log.Error(fmt.Sprintf("err: %s", err))
		req.Status = 777
		req.Msg = fmt.Sprintf("err: %s", err)
		cmn.Resp(w, req)
		return
	}
	fmt.Println(string(buf))
	w.Write(buf)
	return

}
